import React, { lazy, Suspense, useEffect, useState } from 'react';
import { BrowserRouter, Link, Navigate, Route, Routes, useRoutes } from 'react-router-dom';
import { NavBar } from './core/NavBar';

// import SimpleCrud from './pages/crud/SimpleCrud';
import { UiKitDemo } from './pages/uikit/UiKitDemo';
import { SimpleCrudDetails } from './pages/crud/SimpleCrudDetails';
// import { Login } from './pages/login/Login';
import { SignIn } from './pages/login/pages/SignIn';
import { Registration } from './pages/login/pages/Registration';
import { LostPass } from './pages/login/pages/LostPass';
import { Admin } from './pages/admin/Admin';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { PerformanceDemo } from './pages/performance-demo/PerformanceDemo';
import { StateExample1 } from './pages/state-management/StateExample1';
import { StateExample2 } from './pages/state-management/StateExample2';
import { StateExample3 } from './pages/state-management/StateExample3';
import { StateExample4Context } from './pages/state-management/StateExample4Context';
import { StateExample5ContextUseReducer } from './pages/state-management/StateExample5ContextUseReducer';
import { ZustandExample } from './pages/state-management/ZustandExample';
import { JotaiExample } from './pages/state-management/JotaiExample';

const SimpleCrud = lazy(() => import('./pages/crud/SimpleCrud'))
const Login = lazy(() => import('./pages/login/Login'))

const App = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <hr/>
      {/*<MyRoutes />*/}
      <Suspense fallback={<div>loading...</div>}>
         <MyRoutesDeclarive />
      </Suspense>
    </BrowserRouter>
  )
}

export default App;

// ------
export const MyRoutesDeclarive = () => {
  return (<Routes>
    <Route path="admin" element={ <PrivateRoute><Admin /></PrivateRoute>} />
    <Route path="state-example-1" element={<StateExample1 />} />
    <Route path="state-example-2" element={<StateExample2 />} />
    <Route path="state-example-3" element={<StateExample3 />} />
    <Route path="state-example-4-context" element={<StateExample4Context />} />
    <Route path="state-example-5-context-use-reducer" element={<StateExample5ContextUseReducer />} />
    <Route path="zustand" element={<ZustandExample />} />
    <Route path="jotai" element={<JotaiExample />} />
    <Route path="performance" element={<PerformanceDemo />} />
    <Route path="uikit" element={<UiKitDemo />} />
    <Route path="simplecrud" element={<SimpleCrud />} />

    <Route path="login" element={<Login />}>
      <Route path="registration" element={<Registration />} />
      <Route path="lostpass" element={<LostPass />} />
      <Route index element={<SignIn />} />
    </Route>


    <Route path="simplecrud-details/:id" element={<SimpleCrudDetails />} />
    <Route path="/" element={ <Navigate to="simplecrud" /> } />
    <Route path="*" element={
      <div>
        questa pagina non esiste
        <Link to="/">Torna alla home</Link>
      </div>
    } />

  </Routes>)
}




// ------
// Route rules with object
export const MyRoutes = () => {
  return useRoutes([
    { path: 'admin', element: <PrivateRoute><Admin /></PrivateRoute> },
    { path: 'uikit', element: <UiKitDemo /> },
    { path: 'simplecrud', element: <SimpleCrud /> },
    {
      path: 'login',
      element: <Login />,
      children: [
        { path: 'registration', element: <Registration /> },
        { path: 'lostpass', element: <LostPass /> },
        { index: true, element: <SignIn /> },
      ]
    },
    { path: 'simplecrud-details/:id', element: <SimpleCrudDetails /> },
    { path: '/', element: <Navigate to="simplecrud" />  },
    { path: '*', element: (
        <div>
          questa pagina non esiste
          <Link to="/">Torna alla home</Link>
        </div>
      )},
  ])
}
