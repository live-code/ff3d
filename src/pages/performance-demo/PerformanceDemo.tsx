import React, { useCallback, useEffect, useState } from 'react';

const memory = new Set();

export const PerformanceDemo: React.FC = () => {

  const [value, setValue] = useState<number>(0);
  const [data, setData] = useState<number>(0);

  const setTitleHandler = useCallback(() => {
    document.title = 'qualcosa...' + value
  }, [value])

  memory.add(setTitleHandler)
  console.log(' render: Parent', memory)

  return <div>
    <button onClick={() => setValue((s) => s + 1)}>counter {value}</button>
    <button onClick={() => setData((s) => s + 1)}>data {data}</button>
    <button onClick={setTitleHandler}>set title</button>
    <MyCompo onSetTitle={setTitleHandler} />
  </div>
};

const MyCompo = React.memo((props: any) => {
  console.log(' render: MyCompo', props)
  return <div>
    MyCompo
    <button onClick={props.onSetTitle}>change title</button>
  </div>
})
