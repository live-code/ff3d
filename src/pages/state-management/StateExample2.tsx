import React, { useCallback, useState } from 'react';

// AVOID DRILLING PROPS by using COmponent composition
// https://javascript.plainenglish.io/how-to-avoid-prop-drilling-in-react-using-component-composition-c42adfcdde1b
export const StateExample2: React.FC = () => {
  const [value, setValue] = useState(0);
  const [random, setRandom] = useState(123);

  console.log('render: Main')
  const inc = useCallback(() => {
    setValue(s => s + 1)
  }, [])

  return <div>
    Main
    <button onClick={inc}>{value}</button>
    <button onClick={() => setRandom(Math.random)}>random{random}</button>
    <ParentCompo>
      <ChildCompo value={value} inc={inc} />
    </ParentCompo>
  </div>
};

// ====
interface  ParentCompoProps {

}
const ParentCompo: React.FC<ParentCompoProps> = React.memo((props) => {
  console.log(' render: Parent')
  return <div>
    Parent
    {props.children}
  </div>
})

// ====
interface  ChildCompoProps {
  value: number;
  inc: () => void
}
const ChildCompo = React.memo((props: ChildCompoProps) => {
  console.log('  render: Child')
  return <div>
    Child
    <button onClick={props.inc}>{props.value}</button>
  </div>
})
