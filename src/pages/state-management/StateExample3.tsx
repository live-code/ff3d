import React, { useCallback, useReducer, useState } from 'react';

interface State {
  value: number;
  random: number;
}

type DataActionsType = 'increment' | 'setRandom' | 'reset';

type DataActions = {
  type: DataActionsType;
  payload?: number;
}

// useReducer
function reducer(state: State, action: DataActions) {
  switch (action.type) {
    case 'setRandom':
      return { ...state, random: Math.random() * state.value}
    case 'increment':
      return { ...state, value: state.value + 1}
    case 'reset':
      return { value: 0, random: 0}
  }
  return state;
}

export const StateExample3: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, { value: 1, random: 0.18272187})

  return <div>
    <h1>Use Reducer {state.value} - { state.random}</h1>
    <button onClick={() => dispatch({ type: 'setRandom'})}>random</button>
    <button onClick={() => dispatch({ type: 'increment', payload: 10})}>+</button>
    <button onClick={() => dispatch({ type: 'reset'})}>reset</button>

    <MyCompo dispatch={dispatch} />
  </div>
};

const MyCompo = React.memo((props: any) => {
  return <div>
    My Compo
    <button onClick={() => props.dispatch({ type: 'setRandom'})}>random</button>
  </div>
})






// ====
interface  ParentCompoProps {
  value: number;
  inc: () => void
}
function ParentCompoFunc(props: ParentCompoProps) {
  console.log(' render: Parent')
  return <div>
    Parent {props.value}
    <ChildCompo value={props.value} inc={props.inc} />
  </div>
}
export const ParentCompo = React.memo(ParentCompoFunc)

// ====
interface  ChildCompoProps {
  value: number;
  inc: () => void
}
const ChildCompo = React.memo((props: ChildCompoProps) => {
  console.log('  render: Child')
  return <div>
    Child
    <button onClick={props.inc}>{props.value}</button>
  </div>
})
