import React  from 'react';
import { atom, useAtom } from 'jotai';
import { atomWithStorage } from 'jotai/utils';

const themeAtom = atomWithStorage<'dark' | 'light'>('theme', 'dark');
const costAtom = atom<number>(100);
const qtyAtom = atom<number>(3);
const totalAtom = atom<number>((get) => get(costAtom) * get(qtyAtom))

const API = 'https://jsonplaceholder.typicode.com/users'

const urlAtom = atom(`${API}/1`);
const userAtom = atom(
  async (get) => {
    return await fetch(get(urlAtom)).then(res => res.json())
  }
)

export const JotaiExample: React.FC = () => {
  const [data] = useAtom(userAtom);
  const [url, setUrl] = useAtom(urlAtom);

  return <div>
    <h1>Jotai {JSON.stringify(data.name)}</h1>
    <button onClick={() => setUrl(`${API}/2`)}>View User 2</button>
    <hr />
    <Panel1 />
    <Panel2 />
    <Panel3 />
  </div>
};

function Panel3() {
  const [total] = useAtom(totalAtom)
  const [qty, setQty] = useAtom(qtyAtom)

  return <div>
    {total}
    <button onClick={() => setQty(10)}>qty: 10</button>
  </div>
}


const Panel1 = () => {
  const [theme] = useAtom(themeAtom)
  return <div>Panel1 {theme}</div>
}
const Panel2 = () => {
  const [, setTheme] = useAtom(themeAtom)

  return <div>
    Panel2
    <button onClick={() => setTheme('dark')}>dark</button>
    <button onClick={() => setTheme('light')}>light</button>
  </div>
}
