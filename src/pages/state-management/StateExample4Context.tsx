import React, { createContext, useCallback, useContext, useState } from 'react';

interface State {
  value: number;
  random: number;
}

const ValueContext = createContext<number | null>(null)
const RandomContext = createContext<number | null>(null)
// Context
export const StateExample4Context: React.FC = () => {
  return <div>
    Main
    <hr/>
    <ValueCointainer />
    <hr/>
    <RandomCointainer />
  </div>
};


/// VALUE

const ValueCointainer = React.memo(() => {
  const [data, setData] = useState<number>(0);

  console.log(' render: Parent')
  return <ValueContext.Provider value={data}>
    parent
    <button onClick={() => setData(s => s +1)}>+</button>
    <ValueCompo  />
  </ValueContext.Provider>
})

const ValueCompo = () => {
  const data = useContext(ValueContext)
  console.log('  render: value', data)
  return <div>
    VAlue {data}
    <button>value</button>
  </div>
}



/// RANDOM

const RandomCointainer = React.memo(() => {
  const [data, setData] = useState<number>(0);

  console.log(' render: Parent')
  return <RandomContext.Provider value={data}>
    parent
    <button onClick={() => setData(Math.random())}>random</button>
    <RandomCompo  />
  </RandomContext.Provider>
})

const RandomCompo = () => {
  const data = useContext(RandomContext)
  console.log('  render: random', data)

  return <div>
    random {data}
  </div>
}

