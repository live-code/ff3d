import React  from 'react';
import create from 'zustand';
import { devtools } from 'zustand/middleware'

type AppState = {
  counter: number;
  theme: 'light' | 'dark';
  inc: () => void;
  setTheme: (theme: 'dark' | 'light') => void;
}

export const store = create<AppState>(devtools((set) => ({
  counter: 1,
  theme: 'light',
  inc: () => set(s => ({ counter: s.counter + 1})),
  setTheme: (theme: 'dark' | 'light') => set(() => ({ theme })),
})))

export const ZustandExample: React.FC = () => {
  console.log('app render')

  return <div>
    <h1>Zustand</h1>
    <Panel1 />
    <Panel2 />
    <Buttons />
  </div>
};


const Panel1 = () => {
  console.log('panel 1: theme')
  const theme = store(state => state.theme)
  return <div>Panel Theme {theme}</div>
}
const Panel2 = () => {
  console.log('panel 2: counter')
  const counter = store(state => state.counter)
  return <div>Panel Counter {counter}</div>
}

const Buttons = () => {
  const setTheme = store(state => state.setTheme)
  const increment = store(state => state.inc)
  return <div>
    <button onClick={increment}>+</button>
    <button onClick={() => setTheme('light')}>light</button>
    <button onClick={() => setTheme('dark')}>dark</button>

  </div>
}


// work outside react
store.subscribe((res) => console.log(res))
store.setState({ theme: 'dark' })
