import React, { createContext, useCallback, useContext, useReducer, useState } from 'react';
import App from '../../App';

interface State {
  value: number;
  random: number;
}

type DataActionsType = 'increment' | 'setRandom' | 'reset';

type DataActions = {
  type: DataActionsType;
  payload?: number;
}

// useReducer
function reducer(state: State, action: DataActions) {
  switch (action.type) {
    case 'setRandom':
      return { ...state, random: Math.random() * state.value}
    case 'increment':
      return { ...state, value: state.value + 1}
    case 'reset':
      return { value: 0, random: 0}
  }
  return state;
}

type Dispatch = (action: DataActions) => void;
const AppContext = createContext<State | null>(null);
const DispatchContext = createContext<Dispatch>(() => null);

export const StateExample5ContextUseReducer: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, { value: 1, random: 0.18272187})

  return(
      <AppContext.Provider value={state}>
        <DispatchContext.Provider value={dispatch}>
          <h1>Use Reducer {state.value} - { state.random}</h1>
          <button onClick={() => dispatch({ type: 'setRandom'})}>random</button>
          <button onClick={() => dispatch({ type: 'increment', payload: 10})}>+</button>
          <button onClick={() => dispatch({ type: 'reset'})}>reset</button>

          <MyCompo />
        </DispatchContext.Provider>
      </AppContext.Provider>
  )
};

const MyCompo = React.memo((props: any) => {
  return <div>
    My Compo
    <ChildCompo />
  </div>
})


const ChildCompo = React.memo(() => {
  const data = useContext(AppContext);
  const dispatch = useContext(DispatchContext)
  return <div>
    <button onClick={() => dispatch({ type: 'increment', payload: 10})}>{data?.value}</button>
  </div>
})
