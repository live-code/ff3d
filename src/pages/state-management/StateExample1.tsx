import React, { useCallback, useState } from 'react';


// DRILLING PROPS -> passare props da padre a figli
export const StateExample1: React.FC = () => {
  const [value, setValue] = useState(0);
  const [random, setRandom] = useState(123);

  console.log('render: Main')
  const inc = useCallback(() => {
    setValue(s => s + 1)
  }, [])

  return <div>
    Main
    <button onClick={inc}>{value}</button>
    <button onClick={() => setRandom(Math.random)}>random{random}</button>
    <ParentCompo value={value} inc={inc} />
  </div>
};

// ====
interface  ParentCompoProps {
  value: number;
  inc: () => void
}
function ParentCompoFunc(props: ParentCompoProps) {
  console.log(' render: Parent')
  return <div>
    Parent {props.value}
    <ChildCompo value={props.value} inc={props.inc} />
  </div>
}
export const ParentCompo = React.memo(ParentCompoFunc)

// ====
interface  ChildCompoProps {
  value: number;
  inc: () => void
}
const ChildCompo = React.memo((props: ChildCompoProps) => {
  console.log('  render: Child')
  return <div>
    Child
    <button onClick={props.inc}>{props.value}</button>
  </div>
})
