import React  from 'react';
import { useResize } from '../../shared/hooks/useMouse';

export const Admin: React.FC = () => {
  const { w, h } = useResize();

  console.log(w, h)
  return <div>
    Admin
    {
      w > 600 ?
        <div>big</div> :
        <div>small</div>
    } </div>
};
