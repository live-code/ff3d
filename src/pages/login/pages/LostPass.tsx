import React  from 'react';
import { useForm, useFormState } from 'react-hook-form';

interface FormData {
  email: string;
  phone: string;
}
export const LostPass: React.FC = () => {
  const { register, handleSubmit, control } = useForm<FormData>({
    mode: 'onChange',
    reValidateMode: 'onChange'
  })
  const { isDirty, isValid, errors } = useFormState({ control });

  function submitHandler(data: FormData) {
    console.log(data)
  }
  console.log(errors)
  return (
    <div>
      <h1>Lost pass</h1>

      <form onSubmit={handleSubmit(submitHandler)}>
        <input type="text" {...register("email", { required: "This is required." })}/>
        <input type="text" {...register("phone", { required: "This is required." })}/>

        <input type="submit" />
      </form>
    </div>
  )
};
