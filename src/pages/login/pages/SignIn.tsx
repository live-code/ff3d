import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

export const SignIn: React.FC = () => {
  const navigate = useNavigate();
  const userRef = useRef<HTMLInputElement>(null)
  const passRef = useRef<HTMLInputElement>(null)
  const [valid, setValid] = useState<boolean>(false)

  useEffect(() => {
    userRef.current?.focus();

  }, [])

  function signin() {
    if (
      (userRef.current && passRef.current) && valid
    ) {
      console.log(userRef.current.value)
      console.log(passRef.current.value)
    }

    localStorage.setItem('isLogged', 'true')
    // navigate('/uikit')
  }

  function inputHandler() {
    if (
      (userRef.current && passRef.current)
    ) {
      if (userRef.current.value === '' || passRef.current.value === '') {
        setValid(false)
      } else {
        setValid(true)
      }
    }
  }
  console.log('render', valid)

  return <div>
    <input ref={userRef} type="text" placeholder="username" onInput={inputHandler} />
    <input ref={passRef} type="text" placeholder="password" onInput={inputHandler} />

    <button onClick={signin} disabled={!valid}>Signin</button>
  </div>
};
