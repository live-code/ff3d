import React, { FormEvent, useState } from 'react';
import cn from 'classnames';

interface FormData {
  name: string;
  surname: string;
  gender: 'M' | 'F' | '*' | '';
}

const initialState: FormData = { name: '', surname: '', gender: ''};

export const Registration: React.FC = () => {
  const [formData, setFormData] = useState<FormData>(initialState);
  const [dirty, setDirty] = useState<boolean>(false)

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    const key = e.currentTarget.name;
    const value = e.currentTarget.value;
    setFormData({ ...formData, [key]: value });
    setDirty(true);
  }

  const isNameValid = formData.name.length > 3
  const isSurnameValid = formData.surname.length > 3
  const isGenderValid = formData.gender !== '';
  const valid = isNameValid && isSurnameValid && isGenderValid;

  function submitHandler(e: FormEvent) {
    e.preventDefault();
    console.log(formData)
  }

  return (
    <form onSubmit={submitHandler}>
      <pre>{JSON.stringify(dirty, null, 2)}</pre>
      <input type="text" placeholder="name" value={formData.name} name="name"
             onChange={onChangeHandler}
             className={cn(
               'form-control',
               {'is-invalid': !isNameValid && dirty},
               {'is-valid': isNameValid},
             )}
      />
      <input type="text" placeholder="surname" value={formData.surname} name="surname"
             onChange={onChangeHandler}
             className={cn(
               'form-control',
               {'is-invalid': !isSurnameValid && dirty},
               {'is-valid': isSurnameValid},
             )}
      />

      <select
        value={formData.gender} onChange={onChangeHandler} name="gender"
        className={cn(
          'form-control',
          {'is-invalid': !isGenderValid && dirty},
          {'is-valid': isGenderValid},
        )}
      >
        <option value="">Select gender</option>
        <option value="M">male</option>
        <option value="F">female</option>
        <option value="*">*</option>
      </select>

      {isGenderValid && <input type="text"/>}
      <button disabled={!valid}>REGISTER</button>
    </form>
  )
};
