import React  from 'react';
import { Link, Outlet } from 'react-router-dom';

 const Login: React.FC = () => {
  return <div>

    <hr/>

    <Outlet />

    <hr/>

    <Link to="./">signin</Link>
    {' '}
    <Link to="registration">registration</Link>
    {' '}
    <Link to="lostpass">lost pass</Link>

  </div>
};

export default Login;
