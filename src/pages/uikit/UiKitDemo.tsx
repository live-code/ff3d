import React, { useState } from 'react';
import { MapQuest } from '../../shared/MapQuest';
import { GoogleMap } from '../../shared/GoogleMap';
import { Card } from '../../shared/Card';

export const UiKitDemo: React.FC = () => {
  const [zoom, setZoom] = useState<number>(5)
  return <div>
    <button>New York</button>
    <button>London</button>

    <GoogleMap city="London" zoom={zoom} />
    <MapQuest city="London" zoom={zoom} />
    <button onClick={() => setZoom(zoom + 1)}>+</button>
    <button onClick={() => setZoom(zoom - 1)}>-</button>

    <hr/>

    <Card
      title="pippo" success marginBottom
      icon="fa fa-plus"
      onClickIcon={() => {
        console.log('do something')
      }}
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, debitis dolor esse est expedita facilis harum inventore iste laborum magni minus nesciunt obcaecati quas quidem, quisquam reprehenderit sed sequi voluptatum?
    </Card>
  </div>
};
