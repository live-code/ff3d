import React, { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import axios from 'axios';
import { User } from '../../model/user';

export const SimpleCrudDetails: React.FC = () => {
  const { id } = useParams();
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    // console.log(process.env.REACT_APP_API)
    axios.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(res => {
        setUser(res.data)
      })
  }, [id])

  return <div>
    SimpleCrudDetails
    <br/>

    <h1>{user?.name} {id}</h1>
    <NavLink to="/simplecrud-details/4">Next</NavLink>
    {' ' }
    <NavLink to="/simplecrud">back to list</NavLink>
  </div>
};
