import React, { lazy, Suspense } from 'react';
import { UserList } from './components/UserList';
import { useSimpleCrud } from './hooks/useSimpleCrud';
const UserDetails = lazy(() => import('./components/UserDetails'))

const SimpleCrud = () => {
  const {
    users, activeUser, inputEl,
    actions
  } = useSimpleCrud()

  return (
    <div>

      <input ref={inputEl} type="text" onKeyDown={actions.addUser} placeholder="add user name"/>

      <div className="row">
        <div className="col">
          <UserList items={users} onItemClick={actions.setSelectedUser} />
        </div>

        <div className="col">
          <h1>{activeUser?.name}</h1>
          <Suspense fallback={<div>loading</div>}>
            {activeUser && <UserDetails id={activeUser.id}  />}
          </Suspense>
        </div>
      </div>


    </div>
  )
}

export default SimpleCrud;

