import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { User } from '../../../model/user';

interface UserDetailsProps {
  id: number;
}
 const UserDetails: React.VFC<UserDetailsProps> = (props) => {
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    axios.get<User>(`https://jsonplaceholder.typicode.com/users/${props?.id}`)
      .then(res => {
        setUser(res.data)
      })
  }, [props.id]);

  useEffect(() => {

  }, [user])
  return <div>UserDetails: {user?.address.city}</div>
};

export default UserDetails;
