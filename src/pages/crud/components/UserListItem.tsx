import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { User } from '../../../model/user';

interface UserListItem {
  user: User;
  onItemClick: (user: User) => void;
}
export const UserListItem: React.FC<UserListItem> = (props) => {
  const { user } = props;
  const [open, setOpen] = useState<boolean>(false);

  function setActiveUserHandler(e: React.MouseEvent<HTMLElement>, user: User) {
    e.stopPropagation();
    props.onItemClick(user);
  }

  return (
    <li
      className="list-group-item"
      onClick={e => setActiveUserHandler(e, user)}
    >
      <div>{user.name}</div>
      <Link to={`/simplecrud-details/${user.id}`}>
        <i className="fa fa-info-circle" />
      </Link>

      <i className="fa fa-arrow-circle-down"
         onClick={() => setOpen(!open)} />
      {
        open && (
          <div>
            {user.email}
            {user.address.city}
          </div>
        )
      }
    </li>
  )
};
