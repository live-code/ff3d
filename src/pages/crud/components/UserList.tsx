import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { User } from '../../../model/user';
import { UserListItem } from './UserListItem';

interface UserListProps {
  items: User[];
  onItemClick: (user: User) => void;
}
export const UserList: React.VFC<UserListProps> = (props) => {


  return (
    <ul>
      {
        props.items.map(user => {
          return <UserListItem
            key={user.id}
            user={user}
            onItemClick={props.onItemClick}
          />
        })
      }
    </ul>
  )
};
