import { get } from '../../../utility/http.service';
import { User } from '../../../model/user';
import React, { useEffect, useRef, useState } from 'react';
import axios from 'axios';

export function useSimpleCrud() {
  const [users, setUsers] = useState<User[]>([])
  const [activeUser, setActiveUser] = useState<User | null>(null);
  const inputEl = useRef<HTMLInputElement>(null)

  useEffect(() => {
    inputEl.current && inputEl.current.focus()
    get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then((res) => {
        setUsers(res.data)
      })
  }, [])

  function addUser(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      axios.post<User>('https://jsonplaceholder.typicode.com/users', {
        name: e.currentTarget.value
      })
        .then(res => {
          setUsers([...users, res.data])
        })
    }
  }

  function setSelectedUser(user: User) {
    // ...
    setActiveUser(user);
  }


  return {
    actions: {
      addUser,
      setSelectedUser,
    },
    activeUser,
    users,
    inputEl,

  }
}
