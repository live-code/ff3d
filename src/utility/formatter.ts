export function toJSON(val: any) {
  return JSON.stringify(val, null, 2)
}
