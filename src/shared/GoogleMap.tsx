import L from 'leaflet';
import React, { useEffect, useRef } from 'react';

interface GoogleMapProps {
  zoom: number;
  city: string;
}
export const GoogleMap: React.VFC<GoogleMapProps> = (props) => {
  const host = useRef<HTMLDivElement>(null);
  let map = useRef<any>(null)

  useEffect(() => {
    if (host.current && !map.current) {
      map.current = L.map(host.current).setView([51.505, -0.09], props.zoom);

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map.current);

      L.marker([51.5, -0.09]).addTo(map.current)
        .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
        .openPopup();
    }
  }, [])

  useEffect(() => {
    console.log('set zoom');
    if (map.current) {
      map.current.setZoom(props.zoom)
    }
  }, [props.zoom]);

  return <div ref={host} style={{ width: '100%', height: 300}} />
};
