import React, { useState } from 'react';
import classNames from 'classnames';

interface CardProps {
  title: string;
  success?: boolean;
  danger?: boolean;
  marginBottom?: boolean;
  icon?: string;
  onClickIcon?: () => void;
}

export const Card: React.FC<CardProps> = (
  { title, marginBottom, danger, success, icon, onClickIcon, children}
) => {
  const [opened, setOpened] = useState<boolean>(true)

  return (
    <div
      className="card"
      style={{ marginBottom: marginBottom ? 20 : 0}}
    >
      <div
        className={classNames(
         'card-header',
          { 'bg-success': success },
          { 'bg-danger': danger },
        )}
        onClick={() => setOpened(!opened)}
      >
        {title}
        <div className="pull-right">
          { icon &&
            <i className={icon}
               onClick={onClickIcon} />}
        </div>
      </div>
      { opened && <div className="card-body">{children}</div>}
    </div>
  )
};
