import React, { useEffect } from 'react';

interface MapQuestProps {
  zoom: number;
  city: string;
}
export const MapQuest: React.VFC<MapQuestProps> = (props) => {
  useEffect(() => {
    // init map
  }, [])

  useEffect(() => {
    // setZoom
  }, [props.zoom])
  return <div>
    <img src={`https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${props.city}&size=600,400&zoom=${props.zoom}`} alt=""/>
  </div>
};
