import { useEffect, useState } from 'react';

export function useResize() {
  const [pos, setPos] = useState<{ w: number, h: number}>({w: window.innerWidth, h: window.innerHeight});

  function handler() {
    setPos({
      w: window.innerWidth,
      h: window.innerHeight,
    })
  }
  useEffect(() => {
    window.addEventListener('resize', handler)

    return () => {
      window.removeEventListener('resize', handler)
    }
  }, [])

  return pos;
}
