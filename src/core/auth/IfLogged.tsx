import React  from 'react';

interface IfLoggedProps {
  condition: () => boolean
}

export const IfLogged: React.FC<IfLoggedProps> = (props) => {
  return props.condition()?
    <>{props.children}</> :
    null
};
