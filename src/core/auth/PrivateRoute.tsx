import React  from 'react';

export const PrivateRoute: React.FC = (props) => {
  return !!localStorage.getItem('isLogged') ?
      <div>{props.children}</div> :
    null
};
