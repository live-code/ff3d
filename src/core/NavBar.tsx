import React  from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import { IfLogged } from './auth/IfLogged';
import { store } from '../pages/state-management/ZustandExample';


export const NavBar: React.FC = () => {
  const navigate = useNavigate();
  const theme = store(s => s.theme)

  function setActiveOLD(obj: { isActive: boolean }) {
    const { isActive } = obj;
    return isActive ? 'btn btn-warning': 'btn btn-primary'
  }

  function setActive({ isActive }: { isActive: boolean }) {
    return isActive ? 'btn btn-warning': 'btn btn-primary'
  }

  function goto() {
    navigate('uikit')
  }

  const condition = () => {
    return !!localStorage.getItem('isLogged')
  }

  return <div>

    <IfLogged condition={condition}>
      <NavLink to="admin" className={(obj) => setActive(obj)}>Admin</NavLink>
    </IfLogged>

    <NavLink to="state-example-1" className={(obj) => setActive(obj)}>State Ex1</NavLink>
    <NavLink to="state-example-2" className={(obj) => setActive(obj)}>State Ex2</NavLink>
    <NavLink to="state-example-3" className={(obj) => setActive(obj)}>State Ex3</NavLink>
    <NavLink to="state-example-4-context" className={(obj) => setActive(obj)}>Context 1</NavLink>
    <NavLink to="state-example-5-context-use-reducer" className={(obj) => setActive(obj)}>Context 2 Reducer</NavLink>
    <NavLink to="zustand" className={(obj) => setActive(obj)}>Zustand</NavLink>
    <NavLink to="jotai" className={(obj) => setActive(obj)}>Jotai</NavLink>
    <NavLink to="performance" className={(obj) => setActive(obj)}>Performance</NavLink>
    <NavLink to="login" className={(obj) => setActive(obj)}>Login</NavLink>
    <NavLink to="uikit" className={(obj) => setActive(obj)}>Uikit</NavLink>


    <button onClick={goto}>go to</button>
    {' '}

    <NavLink to="simplecrud" className={setActive}>
      Crud
    </NavLink>

    <button onClick={() => {
      localStorage.removeItem('isLogged')
      navigate('simplecrud')
    }}>Logout</button>

    {theme}
  </div>
};
